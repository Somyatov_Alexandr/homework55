import React from 'react';

const Reset = props => {
  return (
      <div className="card__reset">
        <button className="card__btn" onClick={props.reset}>Reset</button>
      </div>
  );
};

export default Reset;