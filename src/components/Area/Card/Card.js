import React from 'react';

const Card = props => {
  return (
      <div onClick={props.click} className='card__item'>
        <div className={props.classList}>
          <div className="flipper">
            <div className="front">
              ?
            </div>
            <div className="back">
              <div className={props.hasItem ? 'circle' : ''}>
                {props.hasItem}
              </div>
            </div>
          </div>
        </div>
      </div>
  );
};

export default Card;