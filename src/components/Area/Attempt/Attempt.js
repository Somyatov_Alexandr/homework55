import React from 'react';

const Attempt = props => {
  return (
      <div className="card__attempt">
        Попыток: {props.counter}
      </div>
  );
};

export default Attempt;