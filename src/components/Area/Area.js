import React from 'react';
import './Area.css';
import Card from "./Card/Card";
import Reset from "./Reset/Reset";
import Attempt from "./Attempt/Attempt";


const Area = props => {
  let count = 0;
  return (
      <div className="card">
        <div className="card__list">

          {props.cards.map((card) => {
            count++;
            return (
                <Card
                    key={count}
                    hasItem={card.hasItem}
                    click={() => props.click(card.id)}
                    classList={card.show ? 'flip-container hover' : 'flip-container'} />
            )
          })}
        </div>

        <Attempt counter={props.counter}  />
        <Reset reset={props.reset} />
      </div>

  );
};

export default Area;