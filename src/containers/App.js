import React, { Component } from 'react';
import './App.css';
import Area from "../components/Area/Area";

class App extends Component {
  state = {
    cards: [{hasItem: false, show: false, id: 1}],
    attempts: 0
  };

  getRandomArray = () => {
    const cards = [];
    const randIndex = Math.floor(Math.random() * 36);

    for (let i = 0; i < 36; i++){
      cards.push({hasItem: false, show: false, id: i});
    }

    cards[randIndex].hasItem = true;


    this.setState({cards});
  };

  showCard = (id) => {
    const index = this.state.cards.findIndex(card => card.id === id);
    const card = {...this.state.cards[index]};

    card.show = true;

    const cards = [...this.state.cards];

    cards[index] = card;

    let attempts = 0;

    cards.forEach((card) => {
      if (card.show){
        attempts++;
      }
    });
    this.setState({cards, attempts});
  };

  attemptsCounter = () => {
    let attempts = 0;
    this.state.cards.forEach((card) => {
      if (card.show){
        attempts++;
      }
    });

    this.setState({attempts});
  };


  componentDidMount(){
    this.getRandomArray();
  }

  render() {
    return (
        <div className="App">
          <Area
              cards={this.state.cards}
              click={this.showCard}
              reset={() => this.getRandomArray()}
              counter={this.state.attempts}
          />

        </div>
    );
  }
}

export default App;
